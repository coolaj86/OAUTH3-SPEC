# Issuer Implementation - Authorization Dialog

Anything that isn't directly related to the wire protocol of transferring information between parties is implementation detail that is left up to the service provider to determine.

For the sake of creating a standard that can actually be used, however, we provide a reference specification and implementation for the issuer that we believe is reasonable.

## 1. check if user exists

In `/.well-known/oauth3/directives.json` you can find `credential_meta` defined as `api/issuer@oauth3.org/logins/meta/{type}/{id}`.

```
https://api.oauth3.org/api/issuer@oauth3.org/logins/meta/{type}/{id}
```

This is the resource at which you can check if a user exists and what details are necessary for login. Presently all accounts must use device-based login, but there is some old code for secure remote passwords still in place.

## 2. send login code (via email)

In `/.well-known/oauth3/directives.json` you can find `credential_otp` defined as `api/issuer@oauth3.org/otp`.

```
https://api.oauth3.org/api/issuer@oauth3.org/otp
```

This is the resource you use to cause a one-time password to be sent to the user.

## 3. submit login code

In `/.well-known/oauth3/directives.json` you can find `access_token` defined as `api/issuer@oauth3.org/access_token`.

```
https://api.oauth3.org/api/issuer@oauth3.org/access_token
```

This is where the login code can be exchanged for a token that the issuer itself will use.

## 4. save device / submit public key

The issuer client may store its own private key per-device. If so, it must submit its public key to the issuer service.

The key should be submitted with a scope. The key will not be allowed to use a scope beyond that which it has been assigned, regardless of the scopes identified in a token itself. When an application's scope is increased, any keys that do not have a limited scope should be increased as well.

## 5. check for existing scope grants

Before asking the user to accept new scopes from an application (azp), the exist grants must be checked.

In `/.well-known/oauth3/directives.json` you can find `grants` defined as `api/issuer@oauth3.org/grants/{azp}/{sub}`.

```
https://api.oauth3.org/api/issuer@oauth3.org/grants/{azp}/{sub}
```

Any scopes that have already been granted for this application must be presented to the user for confirmation.

## 6. present new scopes

Scopes represent schemas of data and are in the form `{schema}@{domain.tld}` and can be found at `/.well-known/scopes@oauth3.org/{schema}@{domain.tld}.json`

```
https://example.com/.well-known/scopes@oauth3.org/files@example.com.json
```

```
{ "title": "Access files"
, "description": "Allow access to example files and folders, listing, copying, etc"
, "icons": [
    { url: "https://example.com/media/file_access_32x32.png"
    , size: "32x32"
    }
  ]
}
```

TODO a simple and generic way to identify a particular set of capabilities or resources (read, write, execute, and group, item). Example files:rw@example.com or files+rw:615294725@example.com. we shall see.

In this way any issuer can sign a token for any scope of any audience (service provider).

## 7. save decision

The user may elect to grant all of the requested scopes, or only a portion of them. Whichever happens, the issuer must save the user's preferences.

## 8. issue token

Now we're back in the realm of handshaking. Continue to the main README.md